<?php
    require('Animal.php');
    require('Frog.php');
    require('Ape.php');

    $sheep = new Animal("shaun");
    echo $sheep-> name . "<br>";
    echo $sheep-> legs . "<br>";
    echo $sheep-> cold_blooded . "<br>" . "<br>";

    $frog = new Frog("buduk");
    echo $frog-> name . "<br>";
    echo $frog-> legs . "<br>";
    $frog-> jump() . "<br>". "<br>";
    
    
    $sungokong = new Ape("kera sakti");
    echo "<br>" . "<br>";
    echo $sungokong-> name . "<br>";
    echo $sungokong-> legs . "<br>";
    $sungokong->yell(); // "Auooo"
?>